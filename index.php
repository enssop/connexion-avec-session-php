<?php
session_start();

// connexion à la bdd
try {
    $db = new PDO("mysql:host=localhost;dbname=demo_utilisateurs", "demoutilisateurs", "demoutilisateurs");
} catch(PDOException $e) {
    echo 'problème d\'accès à la base de données';
    exit;
}

// on gère la déconnexion
if ( isset($_GET['logout']) ) {
    $_SESSION['logged_in'] = false;
    header("Location: index.php");
}

// on gère la connexion
if ( isset($_POST['login']) && isset($_POST['password']) ) {
    $getUser = $db->prepare("SELECT * FROM utilisateurs WHERE `login` = :login");
    $getUser->execute([
        'login' => $_POST['login'],
    ]);

    if ( $getUser->rowCount() > 0 ) {
        $user = $getUser->fetch();

        if ( $_POST['login'] == $user['login'] && password_verify($_POST['password'], $user['password']) ) {
            $_SESSION['logged_in'] = true;
            header("Location: index.php");
        } else {
            header("Location: index.php?error");
        }
        
    } else {
        header("Location: index.php?error");
    }
}

?><!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion utilisateur</title>
</head>
<body>

<?php
var_dump($_SESSION);

if ( isset($_GET['error']) ) {
    echo 'Problème de connexion!<br>';
}

?>

<?php
if ( $_SESSION['logged_in'] == false ) {
    // formulaire de connexion
    ?>
    <form action="index.php" method="POST">
        login: <input type="text" name="login"><br>
        mot de passe: <input type="password" name="password"><br>
        <input type="submit" value="se connecter">
    </form>
    <?php
} else {
    // lien de déconnexion
    ?>
    <a href="/index.php?logout">Déconnexion</a>
    <?php
}
?>

</body>
</html>